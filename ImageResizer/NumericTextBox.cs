﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace ImageResizer
{
    public class NumericTextBox : TextBox
    {
        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            var c = Convert.ToChar(e.Text);
            e.Handled = !char.IsNumber(c);

            base.OnPreviewTextInput(e);
        }
    }
}