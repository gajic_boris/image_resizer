﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace ImageResizer
{
    public partial class MainWindow
    {
        public delegate void ProcessDelegate();
        private string _directory;
        private string _file;
        private float _progressSteps;
        private int _width;
        private bool _working;

        public MainWindow()
        {
            InitializeComponent();
            Files = new Queue<string>();
            SelectedFolder.Text = "No selected folder";
        }

        public Queue<string> Files { get; set; }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _directory = dialog.SelectedPath;
                SelectedFolder.Text = _directory;
            }
            else
                SelectedFolder.Text = "No selected folder";
        }

        private void ResizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (!_working)
            {
                if (!Directory.Exists(_directory))
                {
                    ShowMessage("Select images directory.");
                    return;
                }

                if (!int.TryParse(WidthTextBox.Text, out _width))
                {
                    ShowMessage("Wrong width parameter.");
                    return;
                }

                BrowseButton.IsEnabled = false;
                _working = true;
                ResizeButton.Content = "Stop";
                StatusMessage.Text = "Resizing...";
                Progress.Value = 0;
                Progress.Visibility = Visibility.Visible;

                var files = Directory.GetFiles(_directory);

                foreach (var file in files)
                {
                    if (!IsImage(Path.GetExtension(file))) continue;
                    Files.Enqueue(file);
                }

                if (!Files.Any())
                {
                    ShowMessage("No image files with this extension.");
                    Stop();
                    return;
                }

                _progressSteps = 100f / Files.Count();

                Processing();
            }
            else
                Stop();
        }

        private void Processing()
        {
            var worker = new ProcessDelegate(Resize);
            var completedCallback = new AsyncCallback(ResizeFinished);

            var async = AsyncOperationManager.CreateOperation(null);
            worker.BeginInvoke(completedCallback, async);
        }

        private void Resize()
        {
            _file = Files.Dequeue();
            Resizer.ResizeImage(_file, _width);
        }

        private void ResizeFinished(IAsyncResult ar)
        {
            var worker = (ProcessDelegate) ((AsyncResult) ar).AsyncDelegate;
            var async = (AsyncOperation) ar.AsyncState;

            // finish the asynchronous operation
            worker.EndInvoke(ar);

            // raise the completed event
            var completedArgs = new AsyncCompletedEventArgs(null, false, null);
            async.PostOperationCompleted(e => OnCompleted((AsyncCompletedEventArgs) e), completedArgs);
        }

        protected void OnCompleted(AsyncCompletedEventArgs e)
        {
            Progress.Value += _progressSteps;
            LogTextBlock.Text = LogTextBlock.Text + "\n" + $"Resized_{_width} - {Path.GetFileNameWithoutExtension(_file)}";

            if (!_working) return;
            if (!Files.Any())
            {
                Stop();
                return;
            }

            Processing();
        }

        private void Stop()
        {
            BrowseButton.IsEnabled = true;
            _working = false;
            Files.Clear();
            ResizeButton.Content = "Resize";
            StatusMessage.Text = "Finished";
            Progress.Visibility = Visibility.Hidden;
        }

        private static void ShowMessage(string message)
        {
            System.Windows.MessageBox.Show(message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void Width_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var currentKey = (char) e.Key;
            e.Handled = char.IsDigit(currentKey);
            //if (e.Key < Key.D0 || e.Key > Key.D9 || e.Key == Key.D)
            //{
            //    e.Handled = true;
            //}
        }

        private static bool IsImage(string extension)
        {
            switch (extension.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".png":
                case ".tif":
                case ".tiff":
                case ".gif":
                case ".wmp":
                    return true;
                default:
                    return false;
            }
        }
    }
}