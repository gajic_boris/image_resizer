using System;
using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageResizer
{
    public static class Resizer
    {
        public static void ResizeImage(string source, int width)
        {
            var extension = Path.GetExtension(source);
            var root = Path.GetDirectoryName(source);
            var fileName = Path.GetFileNameWithoutExtension(source);

            var image = Image.FromFile(source);
            var size = CalculateImageSize(new Size(image.Width, image.Height), width, 0);

            var imageBytes = LoadImageData(source);
            var imageSource = CreateImage(imageBytes, size.Width, size.Height);
            var encodedBytes = GetEncodedImageData(imageSource, extension);

            source = $"{root}/Resized_{width}_{fileName}{extension}";
            SaveImageData(encodedBytes, source);
        }

        private static byte[] LoadImageData(string filePath)
        {
            var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);
            var imageBytes = br.ReadBytes((int) fs.Length);
            br.Close();
            fs.Close();
            return imageBytes;
        }

        private static void SaveImageData(byte[] imageData, string filePath)
        {
            var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            var bw = new BinaryWriter(fs);
            bw.Write(imageData);
            bw.Close();
            fs.Close();
        }

        private static BitmapSource CreateImage(byte[] imageData, int decodePixelWidth, int decodePixelHeight)
        {
            if (imageData == null) return null;

            var result = new BitmapImage();
            result.BeginInit();
            if (decodePixelWidth > 0)
            {
                result.DecodePixelWidth = decodePixelWidth;
            }
            if (decodePixelHeight > 0)
            {
                result.DecodePixelHeight = decodePixelHeight;
            }
            result.StreamSource = new MemoryStream(imageData);
            result.CreateOptions = BitmapCreateOptions.None;
            result.CacheOption = BitmapCacheOption.Default;
            result.EndInit();
            return result;
        }

        private static Size CalculateImageSize(Size sourceSize, int width, int height)
        {
            float percent = 1;
            if (width == 0 && height > 0)
                percent = (height / (float)sourceSize.Height);
            if (width > 0 && height == 0)
                percent = (width / (float)sourceSize.Width);
            if (width > 0 && height > 0)
            {
                var widthPercent = (width / (float)sourceSize.Width);
                var heightPercent = (height / (float)sourceSize.Height);
                percent = widthPercent < heightPercent ? widthPercent : heightPercent;
            }

            var resizedWidth = (int)Math.Round(sourceSize.Width * percent);
            var resizedHeight = (int)Math.Round(sourceSize.Height * percent);

            if (sourceSize.Width < resizedWidth && sourceSize.Height < resizedHeight)
                return sourceSize;

            return new Size(resizedWidth, resizedHeight);
        }

        private static byte[] GetEncodedImageData(ImageSource image, string preferredFormat)
        {
            byte[] result = null;
            BitmapEncoder encoder = null;
            switch (preferredFormat.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    encoder = new JpegBitmapEncoder();
                    break;

                case ".bmp":
                    encoder = new BmpBitmapEncoder();
                    break;

                case ".png":
                    encoder = new PngBitmapEncoder();
                    break;

                case ".tif":
                case ".tiff":
                    encoder = new TiffBitmapEncoder();
                    break;

                case ".gif":
                    encoder = new GifBitmapEncoder();
                    break;

                case ".wmp":
                    encoder = new WmpBitmapEncoder();
                    break;
            }

            if (image is BitmapSource && encoder != null)
            {
                var stream = new MemoryStream();
                encoder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                encoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                result = new byte[stream.Length];
                var br = new BinaryReader(stream);
                br.Read(result, 0, (int) stream.Length);
                br.Close();
                stream.Close();
            }
            return result;
        }
    }
}